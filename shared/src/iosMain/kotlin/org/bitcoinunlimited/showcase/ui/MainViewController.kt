package org.bitcoinunlimited.showcase.ui

import androidx.compose.material3.MaterialTheme
import androidx.compose.ui.window.ComposeUIViewController
import org.bitcoinunlimited.showcase.Greeting

fun MainViewController() = ComposeUIViewController {
    val greeting = Greeting().greet()
    MaterialTheme {
        GreetingScreen("$greeting From MainViewController")
    }
}