package org.bitcoinunlimited.showcase.ui

import androidx.compose.material3.Text
import androidx.compose.runtime.Composable

@Composable
fun GreetingScreen(text: String) {
    Text(text = text)
}
