package org.bitcoinunlimited.showcase

interface Platform {
    val name: String
}

expect fun getPlatform(): Platform