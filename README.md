## Compose multiplatform showcase
Displays the same Compose view in Android and iOS (Alfa)

The Greeting Composable is called:
* From `src/iosMain/<id>/ui/MainViewController.kt` which is called from Swift in `iosApp/iosApp/ComposeContentView.swift` for iOS
* and `app/main/<id>/MainActivity.kt` for Android

## Configuration
How to configure the project

### Required to build and run iOS

1. Install homebrew: `/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"`
2. Install Ruby: `brew install ruby`
3. Install cocaopods:`sudo gem install -n /usr/local/bin cocoapods`

Open `iosApp/iosApp.xcworkspace` in Xcode

Go to iosApp -> Signing and Capabilities -> Add your signing team
